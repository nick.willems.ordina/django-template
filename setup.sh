#!/bin/bash

CURR_DIR=$( cd "$(dirname "$0")" || exit ; pwd -P )

python3 "$CURR_DIR/manage.py" makemigrations
python3 "$CURR_DIR/manage.py" migrate --run-syncdb
