from django.apps import AppConfig


class DefaultModuleConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'default_module'
