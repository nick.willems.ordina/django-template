from django.urls import path
from default_module.views import Index

urlpatterns = [
    path('', Index.as_view()),
    path('index/', Index.as_view()),
]
