from django.shortcuts import render
from django.views import View


class Index(View):
    def get(self, request):
        return render(request=request, template_name='default_module/index.html')
